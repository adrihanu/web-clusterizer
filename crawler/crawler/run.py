"""Runner."""
import tldextract
import time, logging, psycopg2
from psycopg2.extras import DictCursor
from twisted.internet import reactor, task
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging

from spiders.crawlWebsiteSpider import CrawlWebsiteSpider

def crawl_websites():
    """Run crawler for websites with `crawled` column set to `None`."""
    sql = "SELECT * FROM websites_website WHERE crawled is NULL ORDER  BY random();"
    conn = psycopg2.connect(host="database", dbname="clusterizer", user="clusterizer", password="a", cursor_factory=DictCursor)
    cur = conn.cursor()
    cur.execute(sql)
    print("Websites to crawl:", cur.rowcount, flush=True)
    website = cur.fetchone()
    if website:
        elements = tldextract.extract(website['fqdn'])
        CrawlWebsiteSpider.allowed_domains = [elements.domain]
        CrawlWebsiteSpider.start_urls = ['http://'+website['fqdn']]
        d = runner.crawl(CrawlWebsiteSpider, website=website)
        time.sleep(4)
        d.addBoth(lambda _: crawl_websites()) 
    else:
        d = task.deferLater(reactor, 5, crawl_websites)
    cur.close()
    conn.close()
 
def main():
    global runner
    #logging.getLogger('scrapy').propagate = False
    configure_logging()
    runner = CrawlerRunner()
    crawl_websites()
    reactor.run()

if __name__ == '__main__':
    main()
