"""Spider for crawling websites."""
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

class CrawlWebsiteSpider(CrawlSpider):
    """Spider for crawling websites."""
    name = "CrawlWebsiteCrawler"
    rules = [Rule(LinkExtractor(canonicalize=True, unique=True),
                  follow=True, callback="parse_item")]
    custom_settings = {"CLOSESPIDER_ITEMCOUNT": 50,
                       "LOG_LEVEL" : "INFO",
                       "ITEM_PIPELINES" : {"pipelines.WebsiteCrawlerPipeline": 100},
                        "SPIDER_MIDDLEWARES" : {
                                'scrapy.spidermiddlewares.offsite.OffsiteMiddleware': None,
                                'middlewares.DomainOffsiteMiddleware': 500,
                        }
                      }
    def __init__(self, *args, **kwargs):
        CrawlSpider.__init__(self, *args, **kwargs)
        self.website = kwargs['website']

    def parse_item(self, response):
        '''Return SubpageItem containing response.'''
        self.logger.info('Parsing {}'.format(response.url[:60]))
        return {"response": response}
