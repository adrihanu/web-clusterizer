# -*- coding: utf-8 -*-
import re, psycopg2
from bs4 import BeautifulSoup, Comment

class WebsiteCrawlerPipeline(object):
    def open_spider(self, spider):
        self.items_saved=[]
        self.sql = "INSERT INTO websites_subpage(url, content, website_id) VALUES (%s, %s, %s);"
        self.conn = psycopg2.connect(host="database", dbname="clusterizer", user="clusterizer", password="a")
        self.cur = self.conn.cursor()
        pass

    def close_spider(self, spider):
        if self.items_saved:
            sql = "UPDATE websites_website SET crawled = %s WHERE id = %s"
            self.cur.execute(sql, (True, spider.website['id']))
            self.conn.commit()
        self.cur.close()
        self.conn.close()

    def process_item(self, item, spider):
        response = item["response"]
        content = self.extract_content(response)
        try:
            self.cur.execute(self.sql, (response.url[:200], content, spider.website['id']))
        except:
            self.cur.execute("ROLLBACK")
        self.conn.commit()
        self.items_saved.append(item)
        return item

    def extract_content(self, response):
        """ Extract text from response"""
        body = response.body.replace(b'\n', b'')#Remove newlines from sentences
        body = body.replace(b'<li', b'\n<li')#Remove newlines from sentences
        body = body.replace(b'<h', b'\n<h')#Remove newlines from sentences
        body = body.replace(b'<br>', b'\n')#Replace <br> with newlines
        elements = BeautifulSoup(body, 'lxml').findAll(text=True)
        visible_elements = []
        hidden_elements_names = ['style', 'script', '[document]', 'head', 'title']

        for element in elements:
            if element.parent.name in hidden_elements_names: continue
            elif isinstance(element, Comment): continue
            visible_elements.append(element)

        content = '' 
        lines = ' '.join(visible_elements).split('\n')
        for line in lines:
            if len(content)>100000: break#TOO LONG
            line = re.sub('\s+', ' ', line).strip()#Remove multiple whitespaces
            alfa = ''.join([l for l in line if l.isalpha()])
            if not alfa: continue#Remove numbers, and labels
            if any(word in line.lower() for word in ['pass', 'log', 'hasł', 'help', 'cookie', 'terms', 'contact']): continue
            content += (' '+line if line[0].islower() else '\n'+line)
        #Remove 4-byte characters
        try:# UCS-4 build
            highpoints = re.compile(u'[\U00010000-\U0010ffff]')
        except re.error:# UCS-2 build
            highpoints = re.compile(u'[\uD800-\uDBFF][\uDC00-\uDFFF]')
        content = highpoints.sub('', content)  
        return content.strip()
