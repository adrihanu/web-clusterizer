Carrot2 Command Line Interface Applications
-------------------------------------------

Carrot2 Command Line Interface Applications enable you to run clustering in batch mode.
To perform clustering of the example data included in the distribution archive type:

batch.cmd input  (on Windows)
./batch.sh input (on Linux)

and look for the "output" directory for the results. Run the script without parameters 
to see the available switches. The CLI applications require a Java Runtime Environment 
(JRE) version 1.7.0 or later. 


For more information, please refer to Carrot2 Manual:
http://download.carrot2.org/stable/manual



Build information
-----------------

Build version : 3.17.0-SNAPSHOT
Build number  : C2-SOFTWARE-CLI-291
Build time    : 2019-01-15 09:39:59
Built by      : bamboo
Build revision: 58c5729f204b1fa848992a7c5fad590b5a64c83c
