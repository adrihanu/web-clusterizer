import threading, subprocess
import logging, psycopg2
import xml.etree.cElementTree as ET
from psycopg2.extras import DictCursor

def printit():

    conn = psycopg2.connect(host="database", dbname="clusterizer", user="clusterizer", password="a", cursor_factory=DictCursor)
    cur = conn.cursor()


    sql = "SELECT * FROM websites_website WHERE crawled is NOT NULL AND clusterized is NULL ORDER  BY random();"
    cur.execute(sql)
    print("Websites to cluster:", cur.rowcount, flush=True)

    try:
        for website in cur.fetchall():
            sql = "SELECT * FROM websites_subpage WHERE website_id=%s;"
            cur.execute(sql, (website['id'],))

            lines = {}
            subpages = [subpage for subpage in list(cur.fetchall())]
            for subpage in subpages:
                for line in subpage['content'].split('\n'):
                    try: lines[line] += 1
                    except KeyError: lines[line] = 1
            
            repeated_lines = [k for k, v in lines.items() if v > 2 and k]

            for subpage in subpages:
                content = subpage['content']
                for line in repeated_lines:
                    content = content.replace(line, '')
                subpage['content'] = content
            
            for algorithm in ['lingo', 'stc', 'kmeans']:
                root = ET.Element("searchresult")
                doc = ET.SubElement(root, "query").text = website['fqdn']

                for subpage in subpages:
                    document = ET.SubElement(root, "document", id=str(subpage['id']))
                    ET.SubElement(document, "snippet").text = subpage['content']
                    ET.SubElement(document, "url").text = subpage['url']

                tree = ET.ElementTree(root)
                tree.write("input/tmp.xml")
                subprocess.call(["./batch.sh", "--algorithm", algorithm, "input/tmp.xml"])

                root = ET.parse("output/tmp.xml")
                groups = root.findall('.//group')
                for group in groups:
                    score = group.get('score')
                    size = group.get('size')
                    
                    phrase = group.find('title').find('phrase').text
                    
                    sql = "INSERT INTO websites_"+algorithm+"cluster(phrase, size, score, website_id) VALUES (%s, %s, %s, %s) ON CONFLICT DO NOTHING;;"       
                    cur.execute(sql, (phrase, size, score, website['id']))

                conn.commit()
            sql = "UPDATE websites_website SET clusterized = %s WHERE id = %s"
            cur.execute(sql, (True, website['id']))
            conn.commit()
    except Exception as e: print(e)

    threading.Timer(5.0, printit).start()
printit()

