# Web clusterizer 

An application for clustering websites using various algorithms. The application receives a website address from the user, then the indexing program is launched to collect some subpages. After collecting and saving the data, a cluster analysis is performed. Project was created during my master's studies.

The application was divided into 5 main parts:

1. Front-end - `JavaScript`(`React.js`) - interacting with the user
2. Backe-end - `Python` (`Django`) - server-side, authentication, database interaction
3. Crawler - `Python` (`Scrapy`) - crawling and collecting subpages
4. Clusterizer - `Java` ([`Carrot2`](https://github.com/carrot2/carrot2)) - cluster analysis with 3 algo: k-means, ling and stc
5. Database - `PostgreSQL` - storing data

Tags: `JavaScript`, `React.js`, `Python`, `Django`, `Scrapy`,`Java`, `Carrot2`, `PostgreSQL`

## Running

```bash
docker-compose up -d
```

## Screens

![png](.img/web_clusterizer_desktop_1.png)


![png](.img/web_clusterizer_desktop_2.png)
