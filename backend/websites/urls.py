from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('websites', views.WebsiteView, base_name='Website')
router.register('subpages', views.SubpageView)

urlpatterns = [
    path('', include(router.urls))
]
