from rest_framework import serializers
from .models import Website, Subpage, LingoCluster, KmeansCluster, StcCluster
from custom_user.models import User

class LingoClusterSerializer(serializers.ModelSerializer):
    class Meta:
        model = LingoCluster
        fields = ('id', 'phrase', 'size', 'score', 'website')

class KmeansClusterSerializer(serializers.ModelSerializer):
    class Meta:
        model = KmeansCluster
        fields = ('id', 'phrase', 'size', 'score', 'website')

class StcClusterSerializer(serializers.ModelSerializer):
    class Meta:
        model = StcCluster
        fields = ('id', 'phrase', 'size', 'score', 'website')

class WebsiteSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all(),
        required=False, 
        allow_null=True, 
        default=None
    )

    def validate_user(self, value):
        return self.context['request'].user

    class Meta:
        model = Website
        fields = ('id', 'fqdn', 'crawled', 'clusterized', 'user')

class WebsiteDetailsSerializer(serializers.ModelSerializer):
    subpages = serializers.StringRelatedField(many=True)
    lingo_clusters = LingoClusterSerializer(many=True, read_only=True)
    kmeans_clusters = KmeansClusterSerializer(many=True, read_only=True)
    stc_clusters = StcClusterSerializer(many=True, read_only=True)

    class Meta:
        model = Website
        fields = ('id', 'fqdn', 'crawled', 'clusterized', 'subpages', 'lingo_clusters', 'stc_clusters', 'kmeans_clusters')

class SubpageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subpage
        fields = ('id', 'url', 'website')
