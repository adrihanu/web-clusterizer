from django.shortcuts import render, get_object_or_404
from rest_framework import viewsets, permissions, mixins
from rest_framework.response import Response
from .models import Website, Subpage, LingoCluster, KmeansCluster, StcCluster
from .serializers import WebsiteSerializer, WebsiteDetailsSerializer, SubpageSerializer, LingoClusterSerializer, KmeansClusterSerializer, StcClusterSerializer
from django.core.exceptions import PermissionDenied

class WebsiteView(mixins.CreateModelMixin, mixins.RetrieveModelMixin, mixins.ListModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = WebsiteSerializer

    def get_queryset(self):
        return Website.objects.filter(user_id=self.request.user.id)

    def retrieve(self, request, pk=None):
        queryset = Website.objects.filter(user_id=request.user.id)
        queryset = get_object_or_404(queryset, pk=pk)
        serializer = WebsiteDetailsSerializer(queryset)
        return Response(serializer.data)
class SubpageView(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Subpage.objects.all()
    serializer_class = SubpageSerializer

class LingoClusterView(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = LingoCluster.objects.all()
    serializer_class = LingoClusterSerializer

class KmeansClusterView(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = KmeansCluster.objects.all()
    serializer_class = KmeansClusterSerializer

class StcClusterView(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = StcCluster.objects.all()
    serializer_class = StcClusterSerializer
