from django.db import models
from custom_user.models import User
class TruncatingCharField(models.CharField):
    def get_prep_value(self, value):
        value = super(TruncatingCharField,self).get_prep_value(value)
        if value:
            return value[:self.max_length]
        return value

class Website(models.Model):
    fqdn = TruncatingCharField(max_length=200)
    crawled = models.NullBooleanField(null=True)
    clusterized = models.NullBooleanField(null=True)
    user = models.ForeignKey(User, related_name='websites', on_delete=models.CASCADE, null=True) 
    
    created_date = models.DateTimeField(auto_now_add=True, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True)
    def __str__(self):
        return self.fqdn

class Subpage(models.Model):
    url = TruncatingCharField(max_length=200)
    content = TruncatingCharField(max_length=100000)
    website = models.ForeignKey(Website, related_name='subpages', on_delete=models.CASCADE) 
    
    created_date = models.DateTimeField(auto_now_add=True, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True)
    def __str__(self):
        return self.url

class LingoCluster(models.Model):
    phrase = TruncatingCharField(max_length=200)
    size = models.FloatField(null=True)
    score = models.FloatField(null=True)
    website = models.ForeignKey(Website, related_name='lingo_clusters', on_delete=models.CASCADE) 
    
    created_date = models.DateTimeField(auto_now_add=True, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        unique_together = ('phrase', 'website')

class StcCluster(models.Model):
    phrase = TruncatingCharField(max_length=200)
    size = models.FloatField(null=True)
    score = models.FloatField(null=True)
    website = models.ForeignKey(Website, related_name='stc_clusters', on_delete=models.CASCADE) 
    
    created_date = models.DateTimeField(auto_now_add=True, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        unique_together = ('phrase', 'website')

class KmeansCluster(models.Model):
    phrase = TruncatingCharField(max_length=200)
    size = models.FloatField(null=True)
    score = models.FloatField(null=True)
    website = models.ForeignKey(Website, related_name='kmeans_clusters', on_delete=models.CASCADE) 
    
    created_date = models.DateTimeField(auto_now_add=True, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        unique_together = ('phrase', 'website')
