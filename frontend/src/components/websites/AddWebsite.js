import React, { Component } from 'react'

class AddWebsite extends Component {
  state = {
    fqdn: '',
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.addWebsite(this.state);
  }
  render() {
    return (
      <div className="container">
        <form className="white" onSubmit={this.handleSubmit}>
          <h5 className="grey-text text-darken-3">Add a new website</h5>
          <div className="input-field">
            <input type="text" id='fqdn' placeholder="fqdn" onChange={this.handleChange} />
          </div>
          <div><button className="btn green lighten-1">Add</button></div>
        </form>
      </div>
    )
  }
}



export default AddWebsite
