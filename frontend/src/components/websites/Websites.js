import React, {Component} from 'react'
import {Link}  from 'react-router-dom'
import { connect } from "react-redux";
import { removeWebsite, getWebsites } from '../../actions/websiteActions'

class WebsitesList extends Component {
  componentDidMount() {
  this.props.getWebsites()
  setInterval(this.props.getWebsites,8000);
  }
  handleClick = (e) => {
    e.preventDefault();
    this.props.removeWebsite({id: e.target.id});
  }
  
  render() {
    let done = <i className="material-icons">done</i>
    let processing = 
        <div className="preloader-wrapper small active">
          <div className="spinner-layer spinner-green-only">
            <div className="circle-clipper left">
              <div className="circle"></div>
            </div><div className="gap-patch">
              <div className="circle"></div>
            </div><div className="circle-clipper right">
              <div className="circle"></div>
            </div>
          </div>
        </div>
    ;

    return (
      <div className='container'>
      <h3>Websites</h3>
      <table>
        <thead>
          <tr>
              <th>Website</th>
              <th>Crawled</th>
              <th>Clusterized</th>
              <th>Actions</th>
          </tr>
        </thead>

        <tbody>
          
      { 


        this.props.websites && this.props.websites.map(website => {
        return (
        <tr key={website.id}>
          <td>
            <Link  to={"/websites/"+website.id}>
              {website.fqdn}
            </Link>
          </td>
          <td>{website.crawled ? done : processing}</td>
          <td>{website.clusterized ? done : processing}</td>
          <td><button id={website.id} onClick={this.handleClick} className="btn red tiny">Remove</button></td>  
        </tr>
        )
      })}  
    </tbody>
    </table>
 


    <Link to="/websites/add">
          <button className="btn green" style={{"marginBottom": "30px", "marginTop": "30px"}}>Add</button>
    </Link>
    </div>
    )
  }
}

const mapStateToProps = state => ({
  websites: state.websites.websites
});

const mapDispatchToProps = dispatch => {
  return {
    getWebsites: () => dispatch(getWebsites()),
    removeWebsite: (website) => dispatch(removeWebsite(website))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WebsitesList);
