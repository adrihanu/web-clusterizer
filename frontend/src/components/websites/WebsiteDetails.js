import React, {Component} from 'react'
import { connect } from "react-redux";
import { getWebsite } from '../../actions/websiteActions'

import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';

import AnyChart from 'anychart-react';

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

function PrepareClastersForDisplay(clusters) {
    var clustersForDisplay = clusters.map((cluster, index) => {
      var value=0
      if (cluster.score) value = cluster.score;// * cluster.size;
      else value = cluster.size;
      return {x: cluster.phrase, value: value}
    })
    console.log(clustersForDisplay);
    return clustersForDisplay;
}
class WebsiteDetails extends Component {

  handleChange = (event, value) => {
    this.setState({ value });
  };
  state = {
    value: 0,
  };
  componentDidMount() {
    this.props.getWebsite(this.props.match.params.id)
  }

  render() {
    if (this.props.website){
        return (
          <div className="container">
                <h3>{ this.props.website.fqdn }</h3>
                <h4>Clusters</h4>
        
                <Paper square>
                  <Tabs
                    value={this.state.value}
                    indicatorColor="primary"
                    textColor="primary"
                    onChange={this.handleChange}
                  >
                    <Tab label="K-means" />
                    <Tab label="Lingo" />
                    <Tab label="STC" />
                  </Tabs>
                </Paper>

                {this.state.value === 0 && 
                <TabContainer>
                    <AnyChart height={800} 
                      type="tagCloud"
                      fromAngle = {-45}
                      toAngle = {45}
                      data = {PrepareClastersForDisplay(this.props.website.kmeans_clusters)}
                      title="K-means"
                    />
                </TabContainer>}

                {this.state.value === 1 && 
                <TabContainer>
                    <AnyChart height={800} 
                      type="tagCloud"
                      fromAngle = {-45}
                      toAngle = {45}
                      data = {PrepareClastersForDisplay(this.props.website.lingo_clusters)}
                      title="Lingo"
                    />
                </TabContainer>}

                {this.state.value === 2 && 
                <TabContainer>
                    <AnyChart height={800} 
                      type="tagCloud"
                      fromAngle = {-45}
                      toAngle = {45}
                      data = {PrepareClastersForDisplay(this.props.website.stc_clusters)}
                      title="STC"
                    />
                </TabContainer>}




                <h4>Crawled subpages</h4>
                <ul className="collection">
                  {
                    this.props.website.subpages && this.props.website.subpages.map((subpage, index) => {
                      return (
                      <li key={index} className="collection-item">
                          {subpage}
                      </li>
                    )
                  })}
                </ul>
          </div>
        )
    }else return (<div></div>)
  }
}

const mapStateToProps = state => ({
  website: state.websites.website
});

const mapDispatchToProps = dispatch => {
  return {
    getWebsite: (id) => dispatch(getWebsite(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WebsiteDetails);

