import React from "react";

const HomePage = ({ registration_message }) => (
  <div className="container">
    {registration_message && (
      <div className="alert alert-info text-center mt-4" role="alert">
        <strong>{registration_message}</strong>
      </div>
    )}
    <h3 className="text-center mt-4">Klasteryzacja stron internetowych</h3>
    Celem pracy jest stworzenie aplikacji służącej do klasteryzacji wybranych stron internetowych za pomocą różnych algorytmów. 
    Aplikacja będzie pobierała od użytkownika adres wybranej domeny internetowej, następnie uruchomiany będzie program indeksujący mający na celu zebranie informacji z części podstron. Po zebraniu i zapisaniu danych dokonywana będzie analiza skupień, w wyniku której zwracane będą frazy kluczowe.
    Uzyskane wyniki będą prezentowane w postaci graficznej.
  </div>
);

export default HomePage;
