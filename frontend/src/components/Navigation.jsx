import React, { Component } from "react";
import { Link } from 'react-router-dom'
import { NavLink } from 'react-router-dom'

class Navigation extends Component {

  userIsAuthenticated() {
    if (this.props.authenticated) {
      return [
              <div key="1">
                <ul className="right">
                  <li><NavLink to='/websites'>Websites</NavLink></li>
                  <li><NavLink onClick={this.props.logoutAction} to='/signout'>Log Out</NavLink></li>
                </ul>
              </div>
      ];
    }
  }

  userIsNotAuthenticated() {
    if (!this.props.authenticated) {
      return [
                <div key="2">
                <ul className="right">
                  <li><NavLink to='/login'>Login</NavLink></li>
                  <li><NavLink to='/register'>Register</NavLink></li>
                </ul>
                </div>
      ];
    }
  }

  render() {
    return (
    <nav className="nav-wrapper green darken-3">
      <div className="container">
          <Link to='/' className="brand-logo">Web Clusterizer</Link>
          {this.userIsNotAuthenticated()}
          {this.userIsAuthenticated()}
      </div>
    </nav>
    );
  }
}

export default Navigation;
