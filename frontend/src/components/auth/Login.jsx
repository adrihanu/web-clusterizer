import React, { Component } from "react";
import { withFormik } from "formik";
import * as Yup from "yup";
import { Link } from "react-router-dom";

import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";


class InnerLoginForm extends Component {
  render() {
    const {
      values,
      touched,
      errors,
      isSubmitting,
      handleChange,
      handleBlur,
      handleSubmit,
    } = this.props;

    return (
      <div className='container'>
        <h3>
          Sign in
        </h3>
        <form onSubmit={handleSubmit}>
          <TextField
            name="username"
            placeholder="Email / Username"
            type="text"
            value={values.username}
            onChange={handleChange}
            onBlur={handleBlur}
            error={errors.username && touched.username}
            helperText={errors.username && touched.username && errors.username}
            label="Email Address / Username"
          />
          <br />
          <TextField
            name="password"
            placeholder="Enter your password"
            type="password"
            value={values.password}
            onChange={handleChange}
            onBlur={handleBlur}
            error={errors.password && touched.password}
            helperText={errors.password && touched.password && errors.password}
            label="Password"
          />
          <br />
          <Button
            variant="contained"
            type="submit"
            disabled={isSubmitting}
          >
            Submit
          </Button>
        </form>
        <span>Do not have an account?</span>{" "}
        <Link to="/register">
          Register
        </Link>
        <br />
      </div>
    );
  }
}

const EnhancedForm = withFormik({
  mapPropsToValues: () => ({
    username: "",
    password: ""
  }),
  validationSchema: Yup.object().shape({
    username: Yup.string().required("This field is required"),
    password: Yup.string()
      .min(6, "The password must be at least 6 characters")
      .required("Password is required")
  }),
  handleSubmit: (
    { username, password },
    { props, setSubmitting, setErrors }
  ) => {
    props.loginAction({ username, password }).then(response => {
      if (response.non_field_errors) {
        setErrors({ password: response.non_field_errors[0] });
      } else {
        props.authenticateAction(
          response,
          props.dispatch,
          props.location.pathname,
          props.history.push
        );
      }
    });
    setSubmitting(false);
  },
  displayName: "LoginForm" //hlps with react devtools
})(InnerLoginForm);

export const Login = EnhancedForm;
