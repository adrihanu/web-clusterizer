import { connect } from "react-redux";
import AddWebsite from "../../components/websites/AddWebsite.js";
import { addWebsite } from '../../actions/websiteActions'

const mapDispatchToProps = dispatch => {
  return {
    addWebsite: (website) => dispatch(addWebsite(website))
  }
}

export default connect(null, mapDispatchToProps)(AddWebsite)
