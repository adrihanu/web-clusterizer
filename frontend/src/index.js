import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import { Router, Route, Switch, Redirect } from "react-router-dom";
import { createBrowserHistory } from 'history';

import { store } from "./store";
import { AuthenticatedRoute } from "./customRoutes/ProtectedRoutes";

import Navigation from "./containers/NavigationContainer";
import HomePage from "./containers/HomePageContainer";
import Login from "./containers/auth/LoginContainer";
import Register from "./containers/auth/RegisterContainer";
import ChangePassword from "./containers/auth/ChangePasswordContainer";
import AddWebsite from './containers/websites/AddWebsite'
import WebsiteDetails from './components/websites/WebsiteDetails'
import WebsiteList from './components/websites/Websites'

export const history = createBrowserHistory();

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <div>
        <Navigation />
        <Switch>
          <Route exact path="/" component={HomePage} />
          <AuthenticatedRoute exact path="/login" component={Login} />
          <AuthenticatedRoute exact path="/register" component={Register} />
          <Route exact path="/signout" render={() => <Redirect to="/" />} />
          <Route exact path="/changepassword" component={ChangePassword} />
          <Route exact path='/websites' component={WebsiteList} />
          <Route exact path='/websites/add' component={AddWebsite} />
          <Route exact path='/websites/:id' component={WebsiteDetails} />
        </Switch>
      </div>
    </Router>
  </Provider>,
  document.getElementById("root")
);
