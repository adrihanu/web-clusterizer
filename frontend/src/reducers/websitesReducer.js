const initState = {
  websites: []
}

const websiteReducer = (state = initState, action) => {
  switch (action.type) {
    case 'ADD_WEBSITE':
      break
    case 'REMOVE_WEBSITE':
      var websites = state.websites.filter(function( website ) { return website.id != action.website.id;});
      return { ...state, websites: websites };
    case 'GET_WEBSITES':
      return { ...state, websites: action.websites };
    case 'GET_WEBSITE':
      return { ...state, website: action.website };
    default:
        // do nothing
  }
  return state;
};

export default websiteReducer;
