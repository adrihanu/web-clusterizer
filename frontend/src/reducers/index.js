import { combineReducers } from "redux";

import authReducer from "./auth/authReducer";
import changePasswordReducer from "./auth/changePasswordReducer";
import websitesReducer from "./websitesReducer";

const rootReducer = combineReducers({
  auth: authReducer,
  websites: websitesReducer, 
  change_password: changePasswordReducer
});

export default rootReducer;
