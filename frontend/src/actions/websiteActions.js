import axios from 'axios';
import { history } from "../index.js";

let url = process.env.REACT_APP_BACKEND_URL;

export const addWebsite = (website) => {
  return (dispatch, getState) => {
    const token = localStorage.getItem("ecom_token");
    website.fqdn = website.fqdn.replace(/(^\w+:|^)\/\//, '');
    axios.post(url+`/websites/`, {fqdn: website.fqdn}, {headers: {"Authorization" : `JWT ${token}`}})
         .then(function (response) {
           dispatch({ type: 'ADD_WEBSITE', website });
           history.push('/websites');
         })
         .catch(function (error) {
           console.log(error);
         });
  }
};

export const removeWebsite = (website) => {
  return (dispatch, getState) => {
    const token = localStorage.getItem("ecom_token");
    axios.delete(url+`/websites/${website.id}`, {headers: {"Authorization" : `JWT ${token}`}})
         .then(function (response) {
           dispatch({ type: 'REMOVE_WEBSITE', website });
           history.push('/websites');
         })
         .catch(function (error) {
           console.log(error);
         });
  }
};

export const getWebsites = () => {
  return (dispatch, getState) => {
    const token = localStorage.getItem("ecom_token");
    axios.get(url+`/websites/`, {headers: {"Authorization" : `JWT ${token}`}})
         .then(function (response) {
           dispatch({ type: 'GET_WEBSITES', websites: response.data});
         })
         .catch(function (error) {
           console.log(error);
         });
  }
};


export const getWebsite = (id) => {
  return (dispatch, getState) => {
    const token = localStorage.getItem("ecom_token");
    axios.get(url+`/websites/${id}/`, {headers: {"Authorization" : `JWT ${token}`}})
         .then(function (response) {
           dispatch({ type: 'GET_WEBSITE', website: response.data});
         })
         .catch(function (error) {
           console.log(error);
         });
  }
};
